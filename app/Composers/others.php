<?php

View::composer(['*'], function($view) {
    $userlist = array();

    try {
        $userlist = App\Models\Users::select('user_id', DB::raw("CONCAT(firstname, ' ',lastname) as name"))
                        ->get()
                        ->lists('name', 'user_id')->toArray();
        $userlist1 = array('' => 'Select Users') + array_filter($userlist);
        $view->with('userlist', $userlist1);
    } catch (\Exception $ex) {
        print_r($ex->getMessage());
        exit;
    }
});

View::composer(['*'], function($view) {
    $postlist = array();

    try {
        $postlist = App\Models\Posts::select('post_id', 'title')
                        ->get()
                        ->lists('title', 'post_id')->toArray();
        $postlist1 = array('' => 'Select Posts') + array_filter($postlist);
        $view->with('postlist', $postlist1);
    } catch (\Exception $ex) {
        print_r($ex->getMessage());
        exit;
    }
});
