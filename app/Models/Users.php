<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Config;

class Users extends Authenticatable {

    public static $multiple_ordering_separator = ",";
    protected $fillable = ['firstname', 'lastname'];
    private $valid_ordering_fields = ["firstname", "lastname", "created_at"];

    public function postdata() {
        return $this->hasMany('App\Models\Posts');
    }

    /**
     * Save user data
     * 
     * @param type $data
     * @return User object or false
     */
    public static function saveUser($data) {

        if (isset($data['user_id']) && !empty($data['user_id'])):
            unset($data['_token']);
            try {
                $data['updated_at'] = date('Y-m-d H:i:s');
                $result = Users::where('user_id', '=', $data['user_id'])->update($data);
            } catch (\Exception $ex) {
                throw new \Exception($ex->getMessage());
            }
            return $data['user_id'];

        else:
            try {
                $insertedId = Users::create($data);
            } catch (\Exception $ex) {
                throw new \Exception($ex->getMessage());
            }
            return $insertedId->id;

        endif;
    }

    /**
     * Delete user data with post or comment
     * 
     * @param type $data
     * @return User object or false
     */
    public static function deleteUsers($id) {
        try {
            $obj = Users::where('user_id', '=', $id);
            $postObj = Posts::where('post_user_id', '=', $id);
            $postdata = $postObj->lists('post_id')->toArray();
            Comments::whereIn('comment_post_id', $postdata)->delete();
            Comments::where('comment_user_id', '=', $id)->delete();
            $postObj->delete();
            $obj->delete();
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
            throw new \Exception('Unable to delete user');
        }
    }

    /**
     * 
     * @param type $search_filters
     * @return type
     */
    public function getall($search_filters = array()) {

        if (!isset($search_filters['sort']) || $search_filters['sort'] == '') {
            $search_filters['sort'] = 'created_at';
            $search_filters['order'] = 'desc';
        }

        if (isset($search_filters['sort']) && $search_filters['sort'] == 'status') {
            $search_filters['sort'] = 'status';
        }

        if (isset($search_filters['sort']) && $search_filters['sort'] == 'first_name') {
            $search_filters['sort'] = 'firstname';
        }

        $q = new Users();
        $q = $this->applySearchFilters($search_filters, $q);
        $q = $this->applyOrderingFilter($search_filters, $q);

        $results_per_page = Config::get('constants.record_per_page');
        return $q->paginate($results_per_page);
    }

    /**
     * @param array $search_filters
     * @param       $q
     * @return mixed
     */
    private function applySearchFilters(array $input_filter = null, $q) {
        if ($this->isSettedInputFilter($input_filter)) {
            foreach ($input_filter as $column => $value) {
                if ($this->isValidFilterValue($value)) {
                    $value = trim($value);
                    switch ($column) {
                        //filter by delete status

                        case 'firstname':
                            $q = $q->where($this->table . '.firstname', 'Like', "%{$value}%");
                            break;
                        //filter by last name
                        case 'lastname':
                            $q = $q->where($this->table . '.lastname', 'Like', "%{$value}%");
                            break;
                    }
                }
            }
        }

        return $q;
    }

    /**
     * @param array $input_filter
     * @return array
     */
    private function isSettedInputFilter(array $input_filter) {
        return $input_filter;
    }

    /**
     * @param $value
     * @return bool
     */
    private function isValidFilterValue($value) {
        return $value !== '';
    }

    /**
     * @param array $input_filter
     * @param       $q
     * @return mixed
     */
    private function applyOrderingFilter(array $input_filter, $q) {
        if ($this->isNotGivenAnOrderingFilter($input_filter))
            return $q;

        foreach ($this->makeOrderingFilterArray($input_filter) as $field => $ordering)
            if ($this->isValidOrderingField($field))
                $q = $this->orderByField($field, $this->guessOrderingType($ordering), $q);

        return $q;
    }

    /**
     * @param array $input_filter
     * @return bool
     */
    private function isNotGivenAnOrderingFilter(array $input_filter) {
        return empty($input_filter['sort']) || empty($input_filter['order']);
    }

    /**
     * @param array $input_filter
     * @return array
     */
    private function makeOrderingFilterArray(array $input_filter) {
        $order_by = explode(static::$multiple_ordering_separator, $input_filter["sort"]);
        $ordering = explode(static::$multiple_ordering_separator, $input_filter["order"]);

        return array_combine($order_by, $ordering);
    }

    /**
     * @param $filter
     * @return bool
     */
    public function isValidOrderingField($ordering_field) {
        return in_array($ordering_field, $this->valid_ordering_fields);
    }

    /**
     * @param array $input_filter
     * @return string
     */
    private function orderByField($field, $ordering, $q) {
        return $q->orderBy($field, $ordering);
    }

    /**
     * @param array $input_filter
     * @return string
     */
    private function guessOrderingType($ordering) {
        return ($ordering == 'desc') ? 'DESC' : 'ASC';
    }

}
