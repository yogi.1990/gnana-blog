<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Config,
    DB;

class Posts extends Authenticatable {

    public static $multiple_ordering_separator = ",";
    protected $fillable = ['post_user_id', 'title', 'body'];
    private $valid_ordering_fields = ["firstname", "title", "created_at"];
    protected $users_table = "users";
    protected $table = "posts";

    public function commentsdata() {
        return $this->hasMany('App\Models\Comments');
    }

    public function userdata() {
        return $this->belongsTo('App\Models\Users', 'post_user_id', 'user_id');
    }

    /**
     * Save Posts data
     * 
     * @param type $data
     * @return User object or false
     */
    public static function savePosts($data) {
        if (isset($data['post_id']) && !empty($data['post_id'])):
            unset($data['_token']);
            try {
                $data['updated_at'] = date('Y-m-d H:i:s');
                $result = Posts::where('post_id', '=', $data['post_id'])->update($data);
            } catch (\Exception $ex) {
                throw new \Exception($ex->getMessage());
            }
            return $data['post_id'];

        else:
            try {
                $insertedId = Posts::create($data);
            } catch (\Exception $ex) {
                throw new \Exception($ex->getMessage());
            }
            return $insertedId->id;

        endif;
    }

    /**
     * Delete post data with post or comment
     * 
     * @param type $data
     * @return User object or false
     */
    public static function deletePosts($id) {
        try {
            Comments::where('comment_post_id', $id)->delete();
            Posts::where('post_id', '=', $id)->delete();
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * 
     * @param type $search_filters
     * @return type
     */
    public function getall($search_filters = array()) {

        if (!isset($search_filters['sort']) || $search_filters['sort'] == '') {
            $search_filters['sort'] = 'created_at';
            $search_filters['order'] = 'desc';
        }

        if (isset($search_filters['sort']) && $search_filters['sort'] == 'status') {
            $search_filters['sort'] = 'status';
        }

        if (isset($search_filters['sort']) && $search_filters['sort'] == 'first_name') {
            $search_filters['sort'] = 'firstname';
        }

        $q = new Posts();
        $q = $q->createTableJoins();
        $q = $this->applySearchFilters($search_filters, $q);
        $q = $this->applyOrderingFilter($search_filters, $q);
//echo $q->toSql();die;
        $results_per_page = Config::get('constants.record_per_page');
        return $q->paginate($results_per_page);
    }

    /**
     * @return mixed
     */
    private function createTableJoins() {
        $q = DB::connection();
        $q = $q->table($this->table)
                ->select([$this->table . '.title',
                    $this->table . '.created_at',
                    $this->table . '.post_id',
                    $this->users_table . '.firstname',
                    $this->users_table . '.lastname'
                ])
                ->join($this->users_table, $this->table . '.post_user_id', '=', $this->users_table . '.user_id');

        return $q;
    }

    /**
     * @param array $search_filters
     * @param       $q
     * @return mixed
     */
    private function applySearchFilters(array $input_filter = null, $q) {
        if ($this->isSettedInputFilter($input_filter)) {
            foreach ($input_filter as $column => $value) {
                if ($this->isValidFilterValue($value)) {
                    $value = trim($value);
                    switch ($column) {
                        case 'firstname':
                            $q = $q->where($this->users_table . '.firstname', 'Like', "%{$value}%");
                            break;

                        case 'lastname':
                            $q = $q->where($this->users_table . '.lastname', 'Like', "%{$value}%");
                            break;

                        case 'title':
                            $q = $q->where($this->table . '.title', 'Like', "%{$value}%");
                            break;
                    }
                }
            }
        }

        return $q;
    }

    /**
     * @param array $input_filter
     * @return array
     */
    private function isSettedInputFilter(array $input_filter) {
        return $input_filter;
    }

    /**
     * @param $value
     * @return bool
     */
    private function isValidFilterValue($value) {
        return $value !== '';
    }

    /**
     * @param array $input_filter
     * @param       $q
     * @return mixed
     */
    private function applyOrderingFilter(array $input_filter, $q) {
        if ($this->isNotGivenAnOrderingFilter($input_filter))
            return $q;

        foreach ($this->makeOrderingFilterArray($input_filter) as $field => $ordering)
            if ($this->isValidOrderingField($field))
                $q = $this->orderByField($field, $this->guessOrderingType($ordering), $q);

        return $q;
    }

    /**
     * @param array $input_filter
     * @return bool
     */
    private function isNotGivenAnOrderingFilter(array $input_filter) {
        return empty($input_filter['sort']) || empty($input_filter['order']);
    }

    /**
     * @param array $input_filter
     * @return array
     */
    private function makeOrderingFilterArray(array $input_filter) {
        $order_by = explode(static::$multiple_ordering_separator, $input_filter["sort"]);
        $ordering = explode(static::$multiple_ordering_separator, $input_filter["order"]);

        return array_combine($order_by, $ordering);
    }

    /**
     * @param $filter
     * @return bool
     */
    public function isValidOrderingField($ordering_field) {
        return in_array($ordering_field, $this->valid_ordering_fields);
    }

    /**
     * @param array $input_filter
     * @return string
     */
    private function orderByField($field, $ordering, $q) {
        return $q->orderBy($field, $ordering);
    }

    /**
     * @param array $input_filter
     * @return string
     */
    private function guessOrderingType($ordering) {
        return ($ordering == 'desc') ? 'DESC' : 'ASC';
    }

}
