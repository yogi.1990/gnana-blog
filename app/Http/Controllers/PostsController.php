<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Posts;
use App\Http\Requests\PostsValidateRequest;
use Input,
    Redirect,
    Config,
    Log;

class PostsController extends Controller {

    /**
     * List forms
     * 
     */
    public function listPosts() {

        $all = Input::all();
        try {
            $postsObj = new Posts();
            $postsData = $postsObj->getall($all);
            return view('posts.list')->with(['postsData' => $postsData]);
        } catch (\Exception $ex) {
            $errors = $ex->getMessage();
            return Redirect::route('posts.index')->withErrors($errors);
        }
    }

    /**
     * Hard delete from
     */
    public function deletePosts() {

        $all = Input::all();
        try {
            $model = new Posts();
            $model->deletePosts(Input::get('id'));
        } catch (\Exception $e) {
            return Redirect::route('posts.index')->withErrors($e->getMessage());
        }
        return Redirect::route('posts.index')->withMessage(Config::get('messages.flash.success.posts_delete_success'));
    }

    /**
     * View form data
     * 
     * @param int $id
     * @return void
     */
    public function viewPosts($id) {

        try {
            $postsData = Posts::where('post_id', $id)->first();
            return view('posts.view')->with(['postsData' => $postsData]);
        } catch (\Exception $e) {
            $errors = $e->getMessage();
            print_r($errors);
            die;
            return Redirect::route('posts.index')->withErrors($errors);
        }
    }

    /**
     * Edit form data
     * 
     * @param int $id
     * @return void
     */
    public function editPosts($id = null) {
        try {

            if ($id) {
                $obj = Posts::where('post_id', '=', $id)->first();
            } else {
                $obj = new Posts();
            }

            return view('posts.form')->with(['postsData' => $obj]);
        } catch (\Exception $e) {
            $errors = $e->getMessage();
            return Redirect::route('posts.index')->withErrors($errors);
        }
    }

    /**
     * Create/Update posts
     * 
     * @method POST
     * @param UserValidateRequest $request request object
     * @return view posts profile
     */
    public function savePosts(PostsValidateRequest $request) {

        $id = Input::get('post_id');
        $formdata = Input::all();

        try {
            $posts = new Posts();
            $posts->savePosts($formdata);
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            print_r($errors);
            die;
            // passing the id incase fails editing an already existing item
            $page = (isset($id) && !empty($id)) ? 'posts.edit' : 'posts.add';
            return Redirect::route($page, $id ? ["id" => $id] : [])->withInput()->withErrors($errors);
        }

        $msg = $id ? 'posts_update_success' : 'posts_create_success';
        return Redirect::route("posts.index")->withMessage(Config::get('messages.flash.success.' . $msg));
    }

}
