<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserValidateRequest;
use App\Models\Users;
use Input,
    Redirect,
    Config,
    Log;

class UsersController extends Controller {

    /**
     * List forms
     * 
     */
    public function listUsers() {

        $all = Input::all();
        try {
            $usersObj = new Users();
            $usersData = $usersObj->getall($all);
            return view('users.list')->with(['usersData' => $usersData]);
        } catch (\Exception $ex) {
            $errors = $ex->getMessage();
            return Redirect::route('users.index')->withErrors($errors);
        }
    }

    /**
     * Hard delete from
     */
    public function delete() {

        $all = Input::all();
        try {
            $model = new Users();
            $model->deleteUsers(Input::get('id'));
        } catch (\Exception $e) {
            return Redirect::route('users.index')->withErrors($e->getMessage());
        }
        return Redirect::route('users.index')->withMessage(Config::get('messages.flash.success.users_delete_success'));
    }

    /**
     * View form data
     * 
     * @param int $id
     * @return void
     */
    public function viewUsers($id) {

        try {
            $usersData = Users::where('user_id', $id)->first();
            return view('users.view')->with(['usersData' => $usersData]);
        } catch (\Exception $e) {
            $errors = $e->getMessage();
            return Redirect::route('users.index')->withErrors($errors);
        }
    }

    /**
     * Edit form data
     * 
     * @param int $id
     * @return void
     */
    public function editUsers($id = null) {
        try {

            if ($id) {
                $obj = Users::where('user_id', '=', $id)->first();
            } else {
                $obj = new Users();
            }

            return view('users.form')->with(['userData' => $obj]);
        } catch (\Exception $e) {
            $errors = $e->getMessage();
            return Redirect::route('users.index')->withErrors($errors);
        }
    }

    /**
     * Create/Update user
     * 
     * @method POST
     * @param UserValidateRequest $request request object
     * @return view user profile
     */
    public function saveUsers(UserValidateRequest $request) { 
        
        $id = Input::get('user_id');
        $formdata = Input::all();

        try {
            $user = new Users();
            $user->saveUser($formdata);
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            // passing the id incase fails editing an already existing item
            $page = (isset($id) && !empty($id)) ? 'users.edit' : 'users.add';
            return Redirect::route($page, $id ? ["id" => $id] : [])->withInput()->withErrors($errors);
        }

        $msg = $id ? 'user_update_success' : 'user_create_success';
        return Redirect::route("users.index")->withMessage(Config::get('messages.flash.success.' . $msg));
    }

}
