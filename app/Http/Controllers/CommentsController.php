<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Comments;
use App\Http\Requests\CommentsValidateRequest;
use Input,
    Redirect,
    Config,
    Log;

class CommentsController extends Controller {

    /**
     * List forms
     * 
     */
    public function listComments() {

        $all = Input::all();
        try {
            $commentsObj = new Comments();
            $commentsData = $commentsObj->getall($all);
            return view('comments.list')->with(['commentsData' => $commentsData]);
        } catch (\Exception $ex) {
            $errors = $ex->getMessage();
            return Redirect::route('comments.index')->withErrors($errors);
        }
    }

    /**
     * Hard delete from
     */
    public function deletePosts() {

        $all = Input::all();
        try {
            $model = new Posts();
            $model->deletePosts(Input::get('id'));
        } catch (\Exception $e) {
            return Redirect::route('posts.index')->withErrors($e->getMessage());
        }
        return Redirect::route('posts.index')->withMessage(Config::get('messages.flash.success.posts_delete_success'));
    }

    /**
     * View form data
     * 
     * @param int $id
     * @return void
     */
    public function viewComments($id) {

        try {
            $commentsData = Comments::where('comment_id', $id)->first();
            return view('comments.view')->with(['commentsData' => $commentsData]);
        } catch (\Exception $e) {
            $errors = $e->getMessage();
            print_r($errors);
            die;
            return Redirect::route('comments.index')->withErrors($errors);
        }
    }

    /**
     * Edit form data
     * 
     * @param int $id
     * @return void
     */
    public function editComments($id = null) {
        try {

            if ($id) {
                $obj = Comments::where('comment_id', '=', $id)->first();
            } else {
                $obj = new Comments();
            }

            return view('comments.form')->with(['commentsData' => $obj]);
        } catch (\Exception $e) {
            $errors = $e->getMessage();
            return Redirect::route('comments.index')->withErrors($errors);
        }
    }

    /**
     * Create/Update comments
     * 
     * @method POST
     * @param UserValidateRequest $request request object
     * @return view comments profile
     */
    public function saveComments(CommentsValidateRequest $request) {

        $id = Input::get('comment_id');
        $formdata = Input::all();

        try {
            $comments = new Comments();
            $comments->savePosts($formdata);
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            print_r($errors);
            die;
            // passing the id incase fails editing an already existing item
            $page = (isset($id) && !empty($id)) ? 'comments.edit' : 'comments.add';
            return Redirect::route($page, $id ? ["id" => $id] : [])->withInput()->withErrors($errors);
        }

        $msg = $id ? 'comments_update_success' : 'comments_create_success';
        return Redirect::route("comments.index")->withMessage(Config::get('messages.flash.success.' . $msg));
    }

}
