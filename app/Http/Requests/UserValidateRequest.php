<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Input,
    Event;

class UserValidateRequest extends Request {

    public function __construct() {
        
    }

    protected $rules = [];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $rules = $this->rules;

        $rules['firstname'] = 'required|max:35|regex:"^[A-Za-z]+$"';
        $rules['lastname'] = 'required|max:35|regex:"^[A-Za-z]+$"';

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    public function messages() {
        return [
            'firstname.required' => 'First name is required.',
            'firstname.max' => 'First name is maximum 35 character',
            'firstname.regex' => 'First name is alphabet only.',
            'lastname.required' => 'Last name is required.',
            'lastname.max' => 'Last name is maximum 35 character',
            'lastname.regex' => 'Last name is alphabet only.',
        ];
    }

}
