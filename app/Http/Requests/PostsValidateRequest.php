<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Input,
    Event;

class PostsValidateRequest extends Request {

    public function __construct() {
        
    }

    protected $rules = [];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $rules = $this->rules;

        $rules['title'] = 'required';
        $rules['body'] = 'required';
        $rules['post_user_id'] = 'required';

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    public function messages() {
        return [
            'post_user_id.required' => 'User name is required.'
        ];
    }

}
