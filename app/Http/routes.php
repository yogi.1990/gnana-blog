<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */


Route::get('/', array(
    "as" => "users.index",
    "uses" => 'UsersController@listUsers'
));

Route::get('users/view/{id}', array(
    "as" => "users.view",
    "uses" => 'UsersController@viewUsers'
));

Route::get('users/add/', array(
    "as" => "users.add",
    "uses" => 'UsersController@editUsers'
));

Route::get('users/edit/{id}', array(
    "as" => "users.edit",
    "uses" => 'UsersController@editUsers'
));

Route::post('users/save/', array(
    "as" => "users.save",
    "uses" => 'UsersController@saveUsers'
));

Route::get('users/delete', array(
    "as" => "users.delete",
    "uses" => 'UsersController@delete'
));

/*
 * Posts
 */
Route::get('/posts', array(
    "as" => "posts.index",
    "uses" => 'PostsController@listPosts'
));

Route::get('posts/view/{id}', array(
    "as" => "posts.view",
    "uses" => 'PostsController@viewPosts'
));

Route::get('posts/add/', array(
    "as" => "posts.add",
    "uses" => 'PostsController@editPosts'
));

Route::get('posts/edit/{id}', array(
    "as" => "posts.edit",
    "uses" => 'PostsController@editPosts'
));

Route::post('posts/save/', array(
    "as" => "posts.save",
    "uses" => 'PostsController@savePosts'
));

Route::get('posts/delete', array(
    "as" => "posts.delete",
    "uses" => 'PostsController@deletePosts'
));


/*
 * Comments
 */
Route::get('/comments', array(
    "as" => "comments.index",
    "uses" => 'CommentsController@listComments'
));

Route::get('comments/view/{id}', array(
    "as" => "comments.view",
    "uses" => 'CommentsController@viewComments'
));

Route::get('comments/add/', array(
    "as" => "comments.add",
    "uses" => 'CommentsController@editComments'
));

Route::get('comments/edit/{id}', array(
    "as" => "comments.edit",
    "uses" => 'CommentsController@editComments'
));

Route::post('comments/save/', array(
    "as" => "comments.save",
    "uses" => 'CommentsController@saveComments'
));

Route::post('comments/delete', array(
    "as" => "comments.delete",
    "uses" => 'CommentsController@deleteComments'
));
