var custom = function () {
    var workonload = function () {

        if ($('#dob').length > 0) {
            $("#dob").datepicker({
                dateFormat: "yy-mm-dd",
                maxDate: 0,
                yearRange: '-100:-18',
                changeMonth: true,
                changeYear: true,
            });
        }

        if ($('.dateset').length > 0) {
            $(".dateset").datepicker({
                dateFormat: "yy-mm-dd",
                changeMonth: true,
                changeYear: true,
            });
        }
    }

    var exportfile = function () {


        if ($('.exportfile1').length > 0) {
            $('.exportfile1 a').click(function () {
                var tabname = $(this).attr('href').split('_')[1];
                $('#namefile1').val(tabname);
            });

        }
        if ($('.exportfile2').length > 0) {
            $('.exportfile2 a').click(function () {
                var tabname = $(this).attr('href').split('_')[1];
                $('#namefile2').val(tabname);
            });
        }
    }

    var uplodefile = function () {


        if ($('#media_file').length > 0) {
            $('body').on('change', '#media_file', function () {
                $('#loader').show();
                $('#is_media').val('1');
                $('form').submit();
            });

        }

        if ($($("#close_popup")).length > 0) {
            $('#close_popup').click(function () {

                if ($('#popup_val').attr('class') == 1) {
                    $("#status_conversation").prop('checked', true); // true
                } else {
                    $("#status_conversation").prop('checked', false); // true
                }

            });
        }

        if ($('#msg_confirm').length > 0) {
            $('#msg_confirm').click(function () {
                var val;
                var rval;
                if ($("#status_conversation").is(':checked')) {
                    val = 1;

                    msg = "Are you sure ticket mark as resolved?";
                } else {
                    val = 0;

                    msg = "Are you sure ticket mark as unresolved?";
                }
                $.ajax({
                    url: baseurl + '/ticket/ticket-status',
                    type: 'get',
                    data: {'status': val, "_token": $('input[type=hidden][name=_token]').val(), 'ticketid': $('input[type=hidden][name=ticket_id]').val()},
                    beforeSend: function () {
                        $('#loader').show();
                    },
                    success: function (data) {
                        $('#loader').hide();
                        // alert(data)
                        $('#popup_val').attr('class', val);
                        $("#close_popup").trigger("click");
                    },
                    error: function () {
                        $('#loader').hide();
                    },
                });
            });
        }


        if ($('#status_conversation').length > 0) {
            $('#status_conversation').click(function () {
                var val;
                if ($(this).is(':checked')) {
                    val = 1;
                    msg = "Are you sure ticket mark as resolved?";
                } else {
                    val = 0;
                    msg = "Are you sure ticket mark as unresolved?";
                }

                $("#pop_up_message").text(msg);
                $("#pop_up").trigger("click");

//                var check = confirm(msg);
//                if (check) {
//                    $.ajax({
//                        url: baseurl + '/ticket/ticket-status',
//                        type: 'get',
//                        data: {'status': val, "_token": $('input[type=hidden][name=_token]').val(), 'ticketid': $('input[type=hidden][name=ticket_id]').val()},
//                        beforeSend: function() {
//                            $('#loader').show();
//                        },
//                        success: function(data) {
//                            $('#loader').hide();
//                            alert(data)
//                        },
//                        error: function() {
//                            $('#loader').hide();
//                        },
//                    });
//                }
            });
        }

    }

    var deletefn = function (el) {
        $("body").on("click", el, function () {
            return confirm("Are you sure to delete this?");
        });
    }
    var softdeletefn = function (el) {
        $("body").on("click", el, function () {
            return confirm("Are you sure to trash this?");
        });
    }
    var restorefn = function (el) {
        $("body").on("click", el, function () {
            return confirm("Are you sure to restore this?");
        });
    }
    var freezefn = function (el) {
        $("body").on("click", el, function () {
            return confirm("Are you sure to freeze this?");
        });
    }
    var unfreezefn = function (el) {
        $("body").on("click", el, function () {
            return confirm("Are you sure to unfreeze this?");
        });
    }

    var popup = function (el) {
        $("body").on("click", el, function () {
            var gid = $(this).attr('id');
            $.ajax({
                url: baseurl + "/bankdetail",
                type: 'get',
                data: {bid: gid},
                beforeSend: function () {
                    $('#loader').show();
                },
                success: function (RESULT) {
                    if (RESULT.length !== 0) {
                        if (RESULT.success != false) {
                           
                            $('#mbody').html(RESULT.html);
                            $('#fullCalModal').modal();
                        } else {
                            $('#radius_error').removeClass('hide').html(RESULT.html);
                        }
                    }
                },
                error: function (jqXhr) {
                    console.log(jqXhr);
                    $('#loader').hide();

                },
                complete: function () {
                    $('#loader').hide();
                }
            });
        });
    }
    return {
        init: function () {
            workonload();
            exportfile();
            uplodefile();
        },
        deletefn: function (el) {
            deletefn(el);
        },
        restorefn: function (el) {
            restorefn(el);
        },
        softdeletefn: function (el) {
            softdeletefn(el);
        },
        freezefn: function (el) {
            freezefn(el);
        },
        unfreezefn: function (el) {
            unfreezefn(el);
        },
        popup: function (el) {
            popup(el);
        },
    }

}();

custom.init();
