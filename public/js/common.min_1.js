var commonJs = function () {
    var init = function () {
        $(window).unload(function () {
            $.ajax({
                url: "/admin/history",
                type: 'GET',
                async: false
            });
        });

        setInterval(function () {
            $.ajax({
                url: "/admin/history",
                type: 'GET',
                async: false
            });
        }, 600000);
        
    }
    var commonsJs = function(){
        $(function(){
            if($('.applyselect2').length > 0){
                $(".applyselect2").select2({});
                $('.select2wosearch').select2({
                    minimumResultsForSearch: -1,
                    dropdownAutoWidth : true
                });
            }
            if(typeof $.validator != 'undefined'){
                $.validator.addMethod("pwcheck", function(value) {
                    return /^(?=.*?[a-z])(?=.*?[0-9]).{8,20}$/.test(value) // consists of only these
                });
                $.validator.addMethod("pwcheckempty", function(value) {
                    if(value.trim()!=''){
                        return /^(?=.*?[a-z])(?=.*?[0-9]).{8,20}$/.test(value) // consists of only these
                    }
                    return true;
                });
                $.validator.addMethod("zipcode", function(value, element) {
                    return this.optional(element) || /^[0-9]{4}/.test(value);
                });
                $.validator.addMethod("phoneNumber", function(value, element) {
                    //var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
                    var filter = /^([0-9]{10})|([0-9]{3}[-]{1}[0-9]{3}[-]{1}[0-9]{4})|([0-9]{3}[.]{1}[0-9]{3}[.]{1}[0-9]{4})|([\(]{1}[0-9]{3}[\)][ ]{1}[0-9]{3}[-]{1}[0-9]{4})|([0-9]{3}[-]{1}[0-9]{4})$/;
                    return this.optional(element) || filter.test(value);
                });
                jQuery.validator.addMethod("phoneno", function(phone_number, element) {
                        //remove spaces
                        //phone_number = phone_number.replace(/\s+/g, "");
                        return this.optional(element) || phone_number.length >= 7 && phone_number.length <= 14 && 
                        //phone_number.match(/^\(?([0-9]{3})\)?([ .-]?)([0-9]{3}|[0-9]{4})\2([0-9]{4})$/);
                        //phone_number.match(/^([0-9]{3})([ .-]?)([0-9]{4}|[0-9]{3})\2([0-9]{4})$/);//10 to 11 digit
                        //phone_number.match(/^(?:[0-9]){6,14}[0-9]$/);
                        phone_number.match(/^[1-9][0-9]{6,14}$/);//7 to 14 digit
                }, "Please specify a valid phone number.");
                $.validator.addMethod("alphanumeric", function(value, element) {
                    return this.optional(element) || /^[A-Za-z]+( *[A-Za-z0-9]*)*$/.test(value);
                    //return this.optional(element) || /^[A-Za-z][A-Za-z0-9 ]+$/.test(value);
                    //return this.optional(element) || /^[A-Za-z\u3300-\u9fff\uf900-\ufaff]+$/.test(value);
                    //return this.optional(element) || /^[A-Za-z]*[A-Za-z0-9]*[\u3300-\u9fff\uf900-\ufaff]*?$/.test(value);
                }, "Name must be alphanumeric and should start with character.");
                $.validator.addMethod("notEmpty", function(value, element, arg){
                    console.log(value);
                    return value != 0;
                }, "Please select field value.");
                jQuery.validator.addMethod("myEmail", function(value, element) {
                    return this.optional( element ) || ( /^[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,4}$/.test( value ) && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test( value ) );
                }, 'Please enter valid email address.');
            }
        })
    }
    return {
        init: function () {
            init();
            commonsJs();
        }

    }

}();

commonJs.init();
