var chartjs = function() {
    var workonload = function() {
        //  alert('fff');
    }

    var piegraph = function(el, doughnutData, legend) {
        var ctx = document.getElementById(el).getContext("2d");
        var myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive: true, legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        });
        document.getElementById(legend).innerHTML = myDoughnut.generateLegend();
    }

    var linegraph = function(el, usergraph, lineChartData) {
        
        $('#' + usergraph).html('<canvas id="canvas" height="200" width="600"></canvas>');
        var ctx = document.getElementById(el).getContext("2d");
        var lineChart = new Chart(ctx).Line(lineChartData, {responsive: true, legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
        });

        //then you just need to generate the legend
        var legend = lineChart.generateLegend();

        //and append it to your page somewhere
        $('#line_legend').html('');
        $('#line_legend').append(legend);
    }

    var DataPicker = function() {
        if ($('#from_graph').length > 0) {
            $('#from_graph').daterangepicker({
                "timePickerIncrement": 1,
                //  "startDate": date,
                //  "endDate": new Date(),
                "opens": "left",
                "drops": "down",
                "maxDate": new Date(),
                "buttonClasses": "btn btn-sm",
                "applyClass": "btn-success",
                "cancelClass": "btn-default",
                locale: {
                    format: 'YYYY-MM-DD'
                },
            }, function(start, end, label) {
                console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
            });
        }
    }

    var graphajax = function() {
        var formData = $("#rgraph").serialize();
        console.log(formData);
        $.ajax({
            url: baseurl + '/dashboard/rgraph',
            type: 'get',
            data: formData,
            dataType: "json",
            beforeSend: function() {
                $('#loader').show();
            },
            success: function(data) {
               
                if ($('#graph_type').val() == 'User') {
                    usergraph = 'wfs';
                } else {
                    usergraph = '';
                }
                console.log(usergraph);
                linegraph("canvas", usergraph, data);
                $('#loader').hide();
            },
            error: function() {
                $('#loader').hide();
            },
        });
    }

    return {
        init: function() {
            var date = new Date();
            date.setMonth(date.getMonth() - 1);
            $(window).load(function() {
                $('#from_graph').val('');
            });
            DataPicker();

            // Graph button rotation
            $('#grph_btn input[name=date]').click(function() {
                $('#grph_btn .myLabel').removeClass('active');
                $(this).parent().addClass('active');
            });

            function dateprocess(date) {
                var parts = date.split("-");
                return new Date(parts[0], parts[1] - 1, parts[2]);
            }
            // 
            if ($('#from_graph').length > 0) {
                $('#search_graph').click(function() {
                    var gedate = $('#from_graph').val();
//    console.log(makeArr);
                    if (gedate != '') {
                        var makeArr = gedate.split(' - ');
                        if (makeArr.length == 2) {
                            $('#from_graph').css({'border': '1px solid #ccc'});
                            var start = dateprocess(makeArr[0]);
                            var end = dateprocess(makeArr[1]);
                            var days = (end - start) / 1000 / 60 / 60 / 24;
                            $('#grph_btn .myLabel').removeClass('active');
                            if (days <= 40) {
                                $('#grph_btn input[value="day"]').parent().addClass('active');
                                $('#grph_btn input[value="day"]').prop("checked", true);
                            } else if (days > 40 && days <= 370) {
                                $('#grph_btn input[value="month"]').parent().addClass('active');
                                $('#grph_btn input[value="month"]').prop("checked", true);
                            } else {
                                $('#grph_btn input[value="year"]').parent().addClass('active');
                                $('#grph_btn input[value="year"]').prop("checked", true);
                            }
                            $('#days').val(days);
                            graphajax();
                        } else {
                            $('#from_graph').css({'border': '1px solid red'});
                        }
                    } else {
                        $('#from_graph').css({'border': '1px solid red'});
                    }
                });
            }

            $('#getcurrent').change(function() {
                $('#from_graph').val('');
                graphajax();
            });

            $('#grph_btn input[name="date"]').change(function() {
                $('#from_graph').val('');
                graphajax();
            });

        },
        piegraph: function(el, doughnutData, legend) {
            piegraph(el, doughnutData, legend);
        },
        linegraph: function(el, usergraph, lineChartData) {
            linegraph(el, usergraph, lineChartData);
        }
    }

}();

chartjs.init();
