<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('comment_id');
            $table->integer('comment_post_id')->unsigned();
            $table->integer('comment_user_id')->unsigned(); 
            $table->longText('message')->nullable(false);
            $table->timestamps();
        });

        Schema::table('comments', function($table) {
            $table->foreign('comment_user_id')->references('user_id')->on('users');
            $table->foreign('comment_post_id')->references('post_id')->on('posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('comments');
    }

}
