<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('post_id');
            $table->integer('post_user_id')->unsigned();
            $table->string('title')->nullable(false);
            $table->longText('body')->nullable(false);
            $table->timestamps();
        });

        Schema::table('posts', function($table) {
            $table->foreign('post_user_id')->references('user_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('posts');
    }

}
