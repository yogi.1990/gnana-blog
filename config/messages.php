<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Flash messages
      |--------------------------------------------------------------------------
      |
     */
    "flash" => [
        /*
         * User success messages
         */
        "success" => [
            'user_create_success' => 'User created with successfully',
            'user_update_success' => 'User updated with successfully',
            'users_delete_success' => 'User deleted with successfully',
            'posts_create_success' => 'Posts created with successfully',
            'posts_update_success' => 'Posts updated with successfully',
            'posts_delete_success' => 'Posts deleted with successfully',
            'comments_create_success' => 'Comments created with successfully',
            'comments_update_success' => 'Comments updated with successfully',
            'comments_delete_success' => 'Comments deleted with successfully',
        // 
        ],
        /*
         * User error messages
         */
        "error" => [
            'record_not_found' => 'Record not found',
        ]
    ]
];
