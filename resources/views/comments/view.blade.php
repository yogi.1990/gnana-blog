@extends('layouts.base')
@section('title') Comments View @stop
@section('container')
<section class="content-header">
    <h1>
        Comments View: 
    </h1> 
</section>
<section class="content">
    <div class="row">
        <div class="box"> 
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <td class="text-center"> First Name : </td>
                        <td class="text-center"> {{isset($commentsData->userdata->firstname) && isset($commentsData->userdata->firstname)?$commentsData->userdata->firstname : ""}} </td>
                    </tr>
                    <tr>
                        <td class="text-center"> Last Name : </td>
                        <td class="text-center"> {{isset($commentsData->userdata->lastname) && isset($commentsData->userdata->lastname)?$commentsData->userdata->lastname : ''}} </td>
                    </tr>
                    <tr>
                        <td class="text-center"> Title : </td>
                        <td class="text-center"> {{isset($commentsData->postdata->title) && isset($commentsData->postdata->title)?$commentsData->postdata->title : ""}} </td>
                    </tr>
                    <tr>
                        <td class="text-center"> Body : </td>
                        <td class="text-center"> {{isset($commentsData->postdata->body) && isset($commentsData->postdata->body)?$commentsData->postdata->body : ''}} </td>
                    </tr>                  
                    <tr>
                        <td class="text-center"> Comment Message : </td>
                        <td class="text-center"> {{isset($commentsData->message) && isset($commentsData->message)?$commentsData->message : ''}} </td>
                    </tr>                  
                    <tr>
                        <td class="text-center"> Created : </td>
                        <td class="text-center">{{ date('d-m-Y', strtotime($commentsData->created_at)) }} </td>
                    </tr> 
                </table>
            </div>


        </div>
        <!-- /.box -->
    </div>

    <!-- /.row -->
</section>
@stop
