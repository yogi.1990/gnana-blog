@extends('layouts.base')
@section('title') Posts  @stop
@section('container')

<section class="content-header">
    <h1>
        {!! Input::get('id') ? 'Edit' : 'Add' !!} Comment : 
    </h1> 
</section> 
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box"> 
                {!! Form::model($commentsData, array('url' => URL::route("comments.save"), 'method' => 'post', 'files' => true, 'class'=>'form-horizontal', 'id'=>'profileform') ) !!}
                <div class="box-body">
                    <div class="col-lg-8 col-md-8 col-xs-12">
                        <div class="form-group required">
                            {!! Form::label('comment_user_id', 'User Name', array('class'=>'col-sm-3 control-label')) !!}
                            <div class="col-sm-9"> 
                                 {!! Form::select('comment_user_id', $userlist, null,array('class'=>'form-control','id'=>'users'));!!}
                                <span class="text-danger">{!! $errors->first('comment_user_id') !!}</span>
                            </div>
                        </div>
                        <div class="form-group required">
                            {!! Form::label('comment_post_id', 'Posts', array('class'=>'col-sm-3 control-label')) !!}
                            <div class="col-sm-9"> 
                                 {!! Form::select('comment_post_id', $postlist, null,array('class'=>'form-control','id'=>'users'));!!}
                                <span class="text-danger">{!! $errors->first('comment_post_id') !!}</span>
                            </div>
                        </div> 
                        <div class="form-group required">
                            {!! Form::label('message', 'Message', array('class'=>'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                {!! Form::textarea('message',null,['class'=>'form-control','placeholder'=>"Message",'id'=>'message'])!!} 
                                <span class="text-danger">{!! $errors->first('message') !!}</span>
                            </div>
                        </div> 
                    </div>

                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        {!! Form::button('<i class="ace-icon fa fa-check bigger-110"></i> Save', array('type' => 'submit', 'class' => 'btn btn-success')) !!}                   
                    </div>
                </div>
                {!! Form::hidden('comment_id',null) !!}
                {{ Form::close() }}
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
@stop
@section('scriptinclude')
{!! HTML::script('/js/jquery.validate.min.js') !!}  
<script type="text/javascript">
    $(document).ready(function () {
        var validator = $('#profileform').validate({
            errorElement: 'span',
            errorClass: 'text-danger',
            errorPlacement: function (error, element) {
                //error.appendTo(element.parent('div').find('span.text-danger'));
                //element.parent('div').find('span.text-danger').text(error.text()).show()
                error.insertAfter(element);
            },
            rules: {
                username: {
                    required: true,
                    alphanumeric: true,
                    maxlength: 20,
                },
            },
            messages: {
                username: {
                    required: "Please enter your Name.",
                    maxlength: "Name should not exceed 20 characters.",
                },
                email: {
                    required: "Email is required.",
                    email: "Please enter a valid email address",
                    remote: "Email address has already been taken.",
                },
            },
            submitHandler: function (form) {
                //pre submit
                form.submit();
            }
        });
    });
</script>
@stop