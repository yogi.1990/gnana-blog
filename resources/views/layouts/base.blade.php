<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>@yield('title')</title>
        <!--<link rel="shortcut icon" href="{{ asset('/backend/img/favicon.ico') }}"/>-->
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        
        {{-- layout style css --}}
        @include('layouts.partials.style')
        
        <script>
            var baseUrl = "{{ Config::get('constantsUrl.WEB_HOST_URL')}}";
        </script>
    </head>
    <body class="skin-blue layout-top-nav">
        <div class="wrapper">
            {{-- common header--}}
            @include('layouts.partials.header')
            {{-- common side bar--}}
            {{--@include('layouts.partials.sidebar')--}}
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <div class="container"><!--row/row_dv/container-->
                <!-- Content Header (Page header) -->
                @yield('container')
                </div>
            </div>
        </div>
        <!-- ./wrapper -->
        
        {{-- layout scripts --}}
        {!! HTML::script('/js/jquery.min.js') !!}
        {!! HTML::script('/js/jquery-ui.min.js') !!}
        <!-- Bootstrap 3.3.7 -->
        {!! HTML::script('/js/bootstrap.min.js') !!}
        {!! HTML::script('/js/common.min.js') !!}
        
        {{-- common script js--}}
        @include('layouts.partials.script')
        
        @yield('scriptinclude')
        @yield('scriptinclude1') 
    </body>
</html>
