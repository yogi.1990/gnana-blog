<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title')</title>
  <link rel="shortcut icon" href="{{ asset('/backend/img/favicon.ico') }}"/>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  {!! HTML::style('/backend/css/bootstrap.min.css') !!}
    {!! HTML::style('/backend/font-awesome/4.4.0/css/font-awesome.min.css') !!}
    {!! HTML::style('/backend/css/AdminLTE.min.css') !!}
    {!! HTML::style('/backend/css/custom.css') !!}
    {!! HTML::style('/backend/js/iCheck/square/blue.css') !!}
    {!! HTML::script('/backend/js/jquery.min.js') !!}
    {!! HTML::script('/backend//js/bootstrap.min.js') !!}
    {!! HTML::script('/backend/js/iCheck/icheck.min.js') !!}


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
    <div class="adminLogin">
        @yield('container') 
    </div>


<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
@yield('scriptinclude')
</body>
</html>
