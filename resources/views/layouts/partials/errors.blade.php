@if($errors && ! $errors->isEmpty() )
@foreach($errors->all() as $error)
<div class="container-fluid">
    <div class="alert alert-danger alert-dismissible">
        <button data-dismiss="alert" class="close" type="button">
            <i class="ace-icon fa fa-times"></i>
        </button>
        {!! $error !!}
    </div>
</div>
@endforeach
@endif