<?php
$curr_act = Route::currentRouteName();
$curr_route_prefix = Route::current()->getPrefix();
?>
<header class="main-header">
    <nav class="navbar navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <a href="{!! URL::route('users.index') !!}" class="navbar-brand"><b>Yogesh</b> Kumar</a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav"> 
                    <li class="<?php echo in_array($curr_act, array('users.index')) ? 'active' : ''; ?>">
                        <a href="{{ URL::route('users.index') }}" ><i class="fa fa-list-alt"></i> Users</a>
                    </li>

                    <li class="<?php echo in_array($curr_route_prefix, array('posts.index')) ? 'active' : ''; ?>">
                        <a href="{{ URL::route('posts.index') }}"><i class="fa fa-dashboard"></i> Posts </a>
                    </li>

                    <li class="<?php echo in_array($curr_route_prefix, array('comments.index')) ? 'active' : ''; ?>">
                        <a href="{{ URL::route('comments.index') }}"><i class="fa fa-dashboard"></i> Comments </a>
                    </li> 
                </ul>

            </div> 
         
        </div>
        <!-- /.container-fluid -->
    </nav>
</header>