<!-- Bootstrap 3.3.7 -->
{!! HTML::style('/css/bootstrap.min.css') !!}  
{!! HTML::style('/css/jquery-ui.min.css') !!}  
<!-- Theme style -->
{!! HTML::style('/css/AdminLTE.min.css') !!}  
<!-- custom style -->
{!! HTML::style('/css/custom.css') !!}  

 
<!-- Font Awesome -->
{!! HTML::style('/font-awesome/4.4.0/css/font-awesome.min.css') !!}  

{!! HTML::style('/css/_all-skins.min.css') !!}  

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">