<?php
$curr_act = Route::currentRouteName();
$curr_route_prefix = Route::current()->getPrefix();
?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar expendSidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">{{--dd(config::get('constantsUrl.NOT_AVALIBLE_IMAGE'))--}}
                <img src="{{ (isset(Auth::guard('admin')->user()->profile_pic) && !empty(Auth::guard('admin')->user()->profile_pic))?asset('/backend/images/users/'.Auth::guard('admin')->user()->profile_pic):asset('/backend/images/default_profile.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><a href="#">{{isset(Auth::guard('admin')->user()->username) && !empty(Auth::guard('admin')->user()->username)?Auth::guard('admin')->user()->username:Auth::guard('admin')->user()->email}}</a></p>
                <a href="javascript:void(0);"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="<?php echo in_array($curr_route_prefix, array('admin/dashboard')) ? 'active' : ''; ?>">
                <a href="{{ URL::route('admin.dashboard.index') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>

            </li>
            <?php $arrForm = array('admin.form.index') ?>
            <li class="treeview <?php echo in_array($curr_route_prefix, array('admin/form')) ? 'active' : ''; ?>" >
                <a href="#">
                    <i class="fa fa-th-list"></i> <span>Form Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class=" <?php echo in_array($curr_act, $arrForm) ?  (Input::get("type") != "trash" ? 'active' : "") : ''; ?>">
                        <a href="{{ URL::route('admin.form.index') }}" ><i class="fa fa-list-alt"></i> Form List</a>
                    </li>
                    <li class=" <?php echo in_array($curr_act, $arrForm) ?  (Input::get("type") != "trash" ? 'active' : "") : ''; ?>">
                        <a href="{{ URL::route('admin.form.index', array('type'=>'trash')) }}" ><i class="fa fa-trash"></i> Trash</a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
