
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
  $(function(){
      $(document).on('click', '.sidebar-toggle', function(e){
          $('.sidebar').toggleClass('expendSidebar', 50);
      });
  });
</script>
{!! HTML::script('/backend/js/daterangepicker.min.js') !!}
{!! HTML::script('/backend/js/adminlte.min.js') !!}        