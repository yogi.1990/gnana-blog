@extends('layouts.base')
@section('title') Users View @stop
@section('container')
<section class="content-header">
    <h1>
        User View: 
    </h1> 
</section>
<section class="content">
    <div class="row">
        <div class="box"> 
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <td class="text-center"> First Name : </td>
                        <td class="text-center"> {{isset($usersData->firstname) && isset($usersData->firstname)?$usersData->firstname : ""}} </td>
                    </tr>
                    <tr>
                        <td class="text-center"> Last Name : </td>
                        <td class="text-center"> {{isset($usersData->lastname) && isset($usersData->lastname)?$usersData->lastname : ''}} </td>
                    </tr>                  
                    <tr>
                        <td class="text-center"> Created : </td>
                        <td class="text-center">{{ date('d-m-Y', strtotime($usersData->created_at)) }} </td>
                    </tr> 
                </table>
            </div>


        </div>
        <!-- /.box -->
    </div>

    <!-- /.row -->
</section>
@stop
