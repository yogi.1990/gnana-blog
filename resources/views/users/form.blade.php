@extends('layouts.base')
@section('title') User  @stop
@section('container')

<section class="content-header">
    <h1>
        {!! Input::get('id') ? 'Edit' : 'Add' !!} User : 
    </h1> 
</section> 

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box"> 
                {!! Form::model($userData, array('url' => URL::route("users.save"), 'method' => 'post', 'files' => true, 'class'=>'form-horizontal', 'id'=>'profileform') ) !!}
                <div class="box-body">
                    <div class="col-lg-8 col-md-8 col-xs-12">
                        <div class="form-group required">
                            {!! Form::label('firstname', 'First Name', array('class'=>'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                {!! Form::text('firstname',null,['class'=>'form-control','placeholder'=>"First Name",'id'=>'firstname'])!!}
                                <span class="text-danger">{!! $errors->first('firstname') !!}</span>
                            </div>
                        </div>
                        <div class="form-group required">
                            {!! Form::label('lastname', 'Last Name', array('class'=>'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                {!! Form::text('lastname',null,['class'=>'form-control','placeholder'=>"Last Name",'id'=>'lastname'])!!}
                                <span class="text-danger">{!! $errors->first('lastname') !!}</span>
                            </div>
                        </div> 
                    </div>

                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        {!! Form::button('<i class="ace-icon fa fa-check bigger-110"></i> Save', array('type' => 'submit', 'class' => 'btn btn-success')) !!}                   
                    </div>
                </div>
                {!! Form::hidden('post_id',null) !!}
                {{ Form::close() }}
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
@stop
@section('scriptinclude')
{!! HTML::script('/js/jquery.validate.min.js') !!}  
<script type="text/javascript">
    $(document).ready(function () {
        var validator = $('#profileform').validate({
            errorElement: 'span',
            errorClass: 'text-danger',
            errorPlacement: function (error, element) {
                //error.appendTo(element.parent('div').find('span.text-danger'));
                //element.parent('div').find('span.text-danger').text(error.text()).show()
                error.insertAfter(element);
            },
            rules: {
                username: {
                    required: true,
                    alphanumeric: true,
                    maxlength: 20,
                },
            },
            messages: {
                username: {
                    required: "Please enter your Name.",
                    maxlength: "Name should not exceed 20 characters.",
                },
                email: {
                    required: "Email is required.",
                    email: "Please enter a valid email address",
                    remote: "Email address has already been taken.",
                },
            },
            submitHandler: function (form) {
                //pre submit
                form.submit();
            }
        });
    });
</script>
@stop