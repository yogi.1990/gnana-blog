@extends('layouts.base')
@section('title') Posts List @stop
@section('container')
<section class="content-header">
    <div>
        @if($errors && ! $errors->isEmpty() )
        @foreach($errors->all() as $error)
        <div class="container-fluid">
            <div class="alert alert-danger alert-dismissible">
                <button data-dismiss="alert" class="close" type="button">
                    <i class="ace-icon fa fa-times"></i>
                </button>
                {!! $error !!}
            </div>
        </div>
        @endforeach
        @endif

        {{-- print messages --}}
        <?php $message = Session::get('message'); ?>
        @if( isset($message) )
        <div class="alert alert-success">{!! $message !!}</div>
        @endif

    </div>
    <h1>
        Posts Management :  <a class="pull-right btn btn-default" href="{{ URL::route("posts.add") }}">Add </a>
    </h1> 
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-9">
            <div class="box"> 
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>                            
                            <th>@sortablelink('firstname', 'First Name')</th>
                            <th>@sortablelink('lastname', 'Last Name')</th>                        
                            <th>@sortablelink('title', 'title')</th>                        
                            <th>@sortablelink('created_at', 'Created')</th>
                            <th>Action</th>
                        </tr>
                        @if(count($postsData) > 0)
                        @foreach($postsData as $key => $posts)
                        <tr> 
                            <td>{{ $posts->firstname }}</td>
                            <td>{{ $posts->lastname }}</td> 
                            <td>{{ $posts->title }}</td> 
                            <td>{{ date('d-m-Y', strtotime($posts->created_at)) }}</td> 
                            <td>
                                <a href="{{ URL::route("posts.view",array("id"=>$posts->post_id)) }}" title="View"> <i class="fa fa-eye"> </i></a> 
                                <a href="{{ URL::route("posts.edit",array("id"=>$posts->post_id)) }}" title="Edit"> <i class="fa fa-pencil"> </i></a> 
                                <a class="delete" title="Delete" href="{{ URL::route("posts.delete",array("id"=>$posts->post_id)) }}"><i class="fa fa-trash"> </i></a>
                                <a title="Comments" href="{{ URL::route("comments.index",array("comment_post_id"=>$posts->post_id)) }}"><i class="fa fa-arrow-right"></i></a>
                            </td>
                        </tr>
                        @endforeach
                        @else

                        <tr>
                            <td colspan="4">{{ Config::get('messages.flash.error.record_not_found')}}</td>
                        </tr>
                        @endif
                    </table>

                </div>

                <div class="box-footer clearfix">
                    {!! $postsData->appends(\Request::except('page'))->render() !!}
                </div>
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-3">
            <!-- general form elements -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-search"></i> Search</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {!! Form::open(['route' => 'posts.index','method' => 'get']) !!}
                <div class="box-body">
                    <div class="form-group"> 
                        {!! Form::label('firstname','First Name:') !!} 
                        {!! Form::text('firstname', null, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}
                    </div>
                    <div class="form-group"> 
                        {!! Form::label('lastname','Last Name:') !!} 
                        {!! Form::text('lastname', null, ['class' => 'form-control', 'placeholder' => 'Last name']) !!} 
                    </div> 
                    <div class="form-group"> 
                        {!! Form::label('title','Title:') !!} 
                        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!} 
                    </div> 
                </div>
                <!-- /.box-body -->

                <div class="box-footer"> 
                    <a href="{!! URL::route('posts.index') !!}" class="btn btn-default search-reset">Reset</a> 
                    {!! Form::submit('Search', ["class" => "btn btn-info pull-right"]) !!}
                </div>

                {!! Form::close() !!}
                </form>
            </div>
            <!-- /.box -->
        </div>
    </div> 
    <!-- /.row -->
</section>
@stop


